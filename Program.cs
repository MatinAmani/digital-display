﻿using System;

class Digit_Matrix
{
    private int[] NUM0 = new int[5] { 7, 5, 5, 5, 7 };
    private int[] NUM1 = new int[5] { 2, 6, 2, 2, 7 };
    private int[] NUM2 = new int[5] { 7, 1, 7, 4, 7 };
    private int[] NUM3 = new int[5] { 7, 1, 7, 1, 7 };
    private int[] NUM4 = new int[5] { 5, 5, 7, 1, 1 };
    private int[] NUM5 = new int[5] { 7, 4, 7, 1, 7 };
    private int[] NUM6 = new int[5] { 7, 4, 7, 5, 7 };
    private int[] NUM7 = new int[5] { 7, 1, 1, 1, 1 };
    private int[] NUM8 = new int[5] { 7, 5, 7, 5, 7 };
    private int[] NUM9 = new int[5] { 7, 5, 7, 1, 7 };
    private int[] OPSum = new int[5] { 0, 2, 7, 2, 0 };
    private int[] OPSub = new int[5] { 0, 0, 7, 0, 0 };
    private int[] OPEqual = new int[5] { 0, 7, 0, 7, 0 };
    private char ch;
    private int[,] A = new int[5, 3];
    private char[,] B = new char[5, 3];

    public Digit_Matrix(char a)
    {
        ch = a;

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                A[i, j] = 0;
            }
        }
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                B[i, j] = ' ';
            }
        }

        int temp;
        switch (ch)
        {
            case '0':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM0[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '1':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM1[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '2':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM2[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '3':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM3[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '4':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM4[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '5':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM5[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '6':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM6[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '7':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM7[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '8':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM8[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '9':
                for (int i = 0; i < 5; i++)
                {
                    temp = NUM9[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '+':
                for (int i = 0; i < 5; i++)
                {
                    temp = OPSum[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '-':
                for (int i = 0; i < 5; i++)
                {
                    temp = OPSub[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            case '=':
                for (int i = 0; i < 5; i++)
                {
                    temp = OPEqual[i];
                    for (int j = 2; j >= 0; j--)
                    {
                        A[i, j] = temp % 2;
                        temp /= 2;
                    }
                }
                break;

            default:
                break;
        }

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (A[i, j] == 1)
                {
                    B[i, j] = ch;
                }
            }
        }
    }
    public void Digit_Print()
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                Console.Write("{0} ", B[i, j]);
            }
            Console.WriteLine();
        }
    }
}
class Program
{
    public static void Main()
    {
        char a, b, c, op;
        Console.WriteLine("Templet: a ± b = c");
        Console.WriteLine("Please note that this program only works for single characters.");
        Console.Write("Enter a: ");
        a = Console.ReadLine()[0];
        Console.Write("Enter Operation( + or - ): ");
        op = Console.ReadLine()[0];
        Console.Write("Enter b: ");
        b = Console.ReadLine()[0];
        Console.Clear();
        int aa, bb, cc = 48;
        aa = a - 48;
        bb = b - 48;
        switch (op)
        {
            case '+':
                cc = aa + bb;
                break;
            case '-':
                cc = aa - bb;
                break;
            default:
                break;
        }
        c = Convert.ToChar(cc + 48);
        Digit_Matrix Digit1 = new Digit_Matrix(a);
        Digit_Matrix Digit2 = new Digit_Matrix(op);
        Digit_Matrix Digit3 = new Digit_Matrix(b);
        Digit_Matrix Digit4 = new Digit_Matrix('=');
        Digit_Matrix Digit5 = new Digit_Matrix(c);
        Digit1.Digit_Print();
        Digit2.Digit_Print();
        Digit3.Digit_Print();
        Digit4.Digit_Print();
        Digit5.Digit_Print();
        Console.ReadKey();
    }
}